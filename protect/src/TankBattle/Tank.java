package TankBattle;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Random;

public class Tank{
    public static final int XSPEED=5,YSPEED=5;
    public static final int WIDTH=30,HEIGHT=30;
    private int x,y,oldX,oldY,life=100;
    private int step;
    private BloodBar bloodBar=new BloodBar();
    private static Random r = new Random();
    Direction[] dirs = Direction.values();
    int rn = r.nextInt(dirs.length);
    public static ArrayList<Missile> listMissil=new ArrayList<>();
    public static ArrayList<Explode> listExplode=new ArrayList<>();
    private boolean bL=false,bU=false,bR=false,bD=false;
    enum Direction{L,LU,U,RU,R,RD,D,LD,STOP}
    private Direction dir=Direction.STOP;
    private Direction ptDir=Direction.D;
    TankClient tc=null;
    private boolean good;
    //private Missile missile;
    private boolean live=true;

    public boolean isGood() {
        return good;
    }

    public void setGood(boolean good) {
        this.good = good;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isLive() {
        return live;
    }

    public void setLive(boolean live) {
        this.live = live;
    }

    public Tank(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public Tank(int x, int y, boolean good) {
        this.x = x;
        this.y = y;
        this.good = good;
    }
    public Tank(int x, int y, boolean good, TankClient tc) {
        this(x, y, good);
        this.tc = tc;
    }

    public int getOldX() {
        return oldX;
    }

    public void setOldX(int oldX) {
        this.oldX = oldX;
    }

    public int getOldY() {
        return oldY;
    }

    public void setOldY(int oldY) {
        this.oldY = oldY;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public void paint(Graphics g) {
        if(!live)return;
        bloodBar.draw(g);
        Color c=g.getColor();
        if(good) {
            g.setColor(Color.RED);
        }
        else {
            g.setColor(Color.BLUE);
        }
        g.fillOval(x,y,WIDTH,HEIGHT);
        g.setColor(Color.black);
        if(!good) locateDiretion();
        move();
        Draw(g);
        g.setColor(c);
    }
    void move(){
        this.oldX = x;
        this.oldY = y;
        switch (dir){
            case L:
                x-=XSPEED;
                break;
            case LU:
                x-=XSPEED;
                y-=YSPEED;
                break;
            case U:
                y-=YSPEED;
                break;
            case RU:
                x+=XSPEED;
                y-=YSPEED;
                break;
            case R:
                x+=XSPEED;
                break;
            case RD:
                x+=XSPEED;
                y+=YSPEED;
                break;
            case D:
                y+=YSPEED;
                break;
            case LD:
                x-=XSPEED;
                y+=YSPEED;
                break;
            case STOP:
                break;
        }
    }
    public void keyPressed(KeyEvent e){
        int key=e.getKeyCode();
        switch (key) {
            case KeyEvent.VK_LEFT:
               bL=true;
                break;
            case KeyEvent.VK_UP:
                bU=true;
                break;
            case KeyEvent.VK_RIGHT:
                bR=true;
                break;
            case KeyEvent.VK_DOWN:
                bD=true;
                break;
            case KeyEvent.VK_CONTROL:
                fire(dir);
                break;
            case KeyEvent.VK_A:
                superFire();
                break;
            }
            locateDiretion();
    }
    public void keyReleased(KeyEvent e){
        int key=e.getKeyCode();
        switch (key) {
            case KeyEvent.VK_LEFT:
                bL=false;
                break;
            case KeyEvent.VK_UP:
                bU=false;
                break;
            case KeyEvent.VK_RIGHT:
                bR=false;
                break;
            case KeyEvent.VK_DOWN:
                bD=false;
                break;
        }
        locateDiretion();
    }
    void locateDiretion(){
        if(good){
            if     ( bL && !bU && !bR && !bD)dir=Direction.L;
            else if( bL &&  bU && !bR && !bD)dir=Direction.LU;
            else if(!bL &&  bU && !bR && !bD)dir=Direction.U;
            else if(!bL &&  bU &&  bR && !bD)dir=Direction.RU;
            else if(!bL && !bU &&  bR && !bD)dir=Direction.R;
            else if(!bL && !bU &&  bR &&  bD)dir=Direction.RD;
            else if(!bL && !bU && !bR &&  bD)dir=Direction.D;
            else if( bL && !bU && !bR &&  bD)dir=Direction.LD;
            else if(!bL && !bU && !bR && !bD)dir=Direction.STOP;
        }
        else{
            Direction[] dirs = Direction.values();
            if(step == 0) {
                step = r.nextInt(12) + 3;
                int rn = r.nextInt(dirs.length); dir = dirs[rn];
            }
            step--;
            if(r.nextInt(40) > 38) Tank.listMissil.add(fire(dir));
        }
    }
    public Missile fire(Direction dir) {
        if(!live) return null;
        int x = this.x + Tank.WIDTH / 2 - Missile.WIDTH / 2;
        int y = this.y + Tank.HEIGHT /2 - Missile.WIDTH / 2;
        Missile m = new Missile(x, y, dir, good);
        Tank.listMissil.add(m);
        return m;
    }
    private void superFire() {
        Direction[] dirs = Direction.values();
        for(int i = 0; i < 8; i++) {
            fire(dirs[i]);
        }
    }
    private void Draw(Graphics g){
        switch (dir){
            case L:
                g.drawLine(x+WIDTH/2,y+HEIGHT/2,x,        y+HEIGHT/2);
                break;
            case LU:
                g.drawLine(x+WIDTH/2,y+HEIGHT/2,x,        y);
                break;
            case U:
                g.drawLine(x+WIDTH/2,y+HEIGHT/2,x+WIDTH/2,y);
                break;
            case RU:
                g.drawLine(x+WIDTH/2,y+HEIGHT/2,x+WIDTH,y);
                break;
            case R:
                g.drawLine(x+WIDTH/2,y+HEIGHT/2,x+WIDTH,y+HEIGHT/2);
                break;
            case RD:
                g.drawLine(x+WIDTH/2,y+HEIGHT/2,x+WIDTH,y+HEIGHT);
                break;
            case D:
                g.drawLine(x+WIDTH/2,y+HEIGHT/2,x+WIDTH/2,y+HEIGHT);
                break;
            case LD:
                g.drawLine(x+WIDTH/2,y+HEIGHT/2,x,y+HEIGHT);
                break;
            case STOP:
                break;
        }
    }
    public boolean collidesWithWall(Wall w) {
        if(this.getRect().intersects(w.getRect()) &&
                this.live) {
            this.stay();
        return true;
        }
        return false;
    }
    public boolean collidesWithTanks(ArrayList<Tank> tanks) {
        for(int i = 0; i < tanks.size(); i++) {
            Tank t = tanks.get(i);
            if(this != t) {
                if(this.live && t.isLive() && this.getRect().intersects(t.getRect())) {
                    t.stay();
                    this.stay();
                    return true;
                }
            }
        }
        return false;
    }
    private void stay() {
        x = oldX;
        y = oldY;
    }
    private class BloodBar {
        public void draw(Graphics g) {
            Color c = g.getColor();
            g.setColor(Color.RED);
            g.drawRect(x, y - 10, WIDTH, 10);
            int w = WIDTH * life / 100;
            g.fillRect(x, y - 10, w, 10);
            g.setColor(c);
        }
    }
    public boolean eat(Blood b) {
        if(this.live && b.isLive() &&
                this.getRect().intersects(b.getRect())) {
            this.life = 100;
            b.setLive(false);
            return true;
        }
        return false;
    }
    public Rectangle getRect() {
        return new Rectangle(x, y, WIDTH, HEIGHT);
    }
}