package TankBattle;

import java.awt.*;
import java.util.ArrayList;

public class Missile {
    public static final int XSPEED = 10, YSPEED = 10;
    public static final int WIDTH = 10, HEIGHT = 10;
    private int x, y;
    Tank.Direction dir;
    private boolean live = true;
    private boolean good;

    public Missile(int x, int y, Tank.Direction dir,Boolean good) {
        this.x = x;
        this.y = y;
        this.dir = dir;
        this.good=good;
    }

    public void paint(Graphics g) {
        if(!live)return;
        Color c = g.getColor();
        g.setColor(Color.BLACK);
        g.fillOval(x, y, 10, 10);
        g.setColor(c);
        move();
    }

    private void move() {
        switch (dir) {
            case L:
                x -= XSPEED;
                break;
            case LU:
                x -= XSPEED;
                y -= YSPEED;
                break;
            case U:
                y -= YSPEED;
                break;
            case RU:
                x += XSPEED;
                y -= YSPEED;
                break;
            case R:
                x += XSPEED;
                break;
            case RD:
                x += XSPEED;
                y += YSPEED;
                break;
            case D:
                y += YSPEED;
                break;
            case LD:
                x -= XSPEED;
                y += YSPEED;
                break;
            case STOP:
                break;
        }
    }


    public Rectangle getRect() {
        return new Rectangle(x, y, WIDTH, HEIGHT);
    }

    public boolean hitTank(Tank t) {
        if (this.live
                && this.getRect().intersects(t.getRect())
                && t.isLive()
                && this.good != t.isGood())
        {
                t.setLife(t.getLife() - 20);
                if(t.getLife()<=0){
                    TankClient.enemyTankList.remove(t);
                    live = false;
                    Explode e = new Explode(t.getX(), t.getY());
                    Tank.listExplode.add(e);
                }

                return true;
        }
        return false;
    }
     public boolean hitTanks(ArrayList < Tank > tanks) {
        for (int i = 0; i < tanks.size(); i++) {
            if (hitTank(tanks.get(i))) {
                return true;
            }
        }
        return false;
    }
    public boolean hitWall(Wall w) {
        if(this.live &&
                this.getRect().
                        intersects(w.getRect())) {
            this.live = false;
            return true;
        }
        return false;
    }
    public boolean collidesWithTank(Tank t) {
        if(this.live && this.getRect().intersects(t.getRect()) &&
                t.isLive() &&
                this.good != t.isGood()) {
            if(t.isGood()) {
                t.setLife(t.getLife() - 20);
                if(t.getLife() <= 0) {
                    t.setLive(false);
                }
            }
            else {
                t.setLive(false);
            }
            this.live = false;
            Explode e = new Explode(x, y);
            Tank.listExplode.add(e);
            return true;
        }
        return false;
    }


    public void setLive ( boolean live){
        this.live = live;
    }
    public boolean isLive () {
        return live;
    }
    public boolean isGood() {
        return good;
    }

    public void setGood(boolean good) {
        this.good = good;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}